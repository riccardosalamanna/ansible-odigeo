#!/bin/bash

HOSTNAME=$(hostname)
JOIN_USER="edo.vdihu"
JOIN_PASSWORD="A@f96647dd"
DOMAIN_FOLDER="/opt/AD-join"
DOMAIN_FILE="${DOMAIN_FOLDER}/.domain_joined"

if [ -f "$DOMAIN_FILE" ]; then
	echo "$HOSTNAME was already added to AD" | systemd-cat -p info
elif grep "$HOSTNAME" /etc/krb5.keytab;then
	echo "$HOSTNAME is already in odigeo.org - skipping" | systemd-cat -p info
elif ping -q -c 1 -W 1 odigeo.org; then
	echo "Adding $HOSTNAME to AD..." | systemd-cat -p info
	rm -f /etc/krb5.keytab
	realm leave odigeo.org
	echo $JOIN_PASSWORD | realm join odigeo.org --user $JOIN_USER --computer-ou="ou=Horizon,ou=VDI,ou=PCs,dc=odigeo,dc=org"
	service sssd stop
	cp "${DOMAIN_FOLDER}/sssd.conf" /etc/sssd/sssd.conf
	service sssd restart
	systemctl is-active --quiet sssd && echo "$HOSTNAME has successfully joined AD - rebooting the system in 5 seconds" | systemd-cat -p info ; sleep 5 ; reboot
	systemctl is-failed --quiet sssd && echo "$HOSTNAME AD join FAILED!!! - proceed manually" | systemd-cat -p err
else
	echo "The domain odigeo.org is not reachable - check your DNS records" | systemd-cat -p err
fi


